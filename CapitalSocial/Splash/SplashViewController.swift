//
//  SplashViewController.swift
//  CapitalSocial
//
//  Created by Alicia Castro on 3/22/20.
//  Copyright © 2020 Alicia Castro. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let loginVC: LoginViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
}
