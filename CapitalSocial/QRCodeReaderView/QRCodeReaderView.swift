//
//  QRCodeReaderView.swift
//  CapitalSocial
//
//  Created by Alicia Castro on 3/23/20.
//  Copyright © 2020 Alicia Castro. All rights reserved.
//

import Foundation

import QRCodeReader

import UIKit

class QRYourCustomView: UIView, QRCodeReaderDisplayable {
    let cameraView: UIView            = UIView()
    let cancelButton: UIButton?       = UIButton()
    let switchCameraButton: UIButton? = SwitchCameraButton()
    let toggleTorchButton: UIButton?  = ToggleTorchButton()
    var overlayView: QRCodeReaderViewOverlay?
//        = QRCodeReaderViewOverlay()
    
    func setNeedsUpdateOrientation() {
        
    }

  func setupComponents(with builder: QRCodeReaderViewControllerBuilder) {
    // addSubviews
    // setup constraints
    // etc.
  }
}
