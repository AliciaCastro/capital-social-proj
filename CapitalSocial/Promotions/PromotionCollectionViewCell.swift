//
//  PromotionCollectionViewCell.swift
//  CapitalSocial
//
//  Created by Alicia Castro on 3/22/20.
//  Copyright © 2020 Alicia Castro. All rights reserved.
//

import UIKit

class PromotionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var promotionName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(_ image: UIImage?) {
        if let image = image {
            self.imageView.image = image
        } else {
            self.imageView.image = #imageLiteral(resourceName: "placeholder")
        }
    }
    
    func setPromotionName(_ name: String) {
        self.promotionName.text = name
    }
}
