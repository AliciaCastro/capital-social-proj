//
//  PromotionsViewController.swift
//  CapitalSocial
//
//  Created by Alicia Castro on 3/22/20.
//  Copyright © 2020 Alicia Castro. All rights reserved.
//

import UIKit

class PromotionsViewController: UIViewController {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    var promotions: [PromotionModel] = []
    let promotionsName = ["Papa Jhon's", 
                        "Idea interior", 
                        "BurgerKing", 
                        "Farmacia Beneavides", 
                        "El Tizoncito", 
                        "Chilli's",
                        "Zona Fitness",
                        "Cinepolis",
                        "Wingstop"]
    let imagesName = ["LogoPapaJohns",
                "",
                "PromoBurguerKing",
                "PromoBenavides",
                "PromoTizoncito",
                "PromoChilis",
                "PromoZonaFitness",
                "PromoCinepolis",
                "PromoWingstop"]
    let dummyDescription =  "Está pensado para albergar pequeñas celebraciones o comidas de empresa, con una capacidad de hasta ochenta personas. Además dispone de proyector y pantalla lo que posibilita su utilización para presentaciones y eventos similares."
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        self.registerCollectionCell()
        self.initializeEntities()
        self.collectionView.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    private func initializeEntities() {
        for i in 0..<9 {
            let image: UIImage? = self.imagesName[i] != "" ? #imageLiteral(resourceName:  self.imagesName[i]) : nil
            let promotion = PromotionModel(name: self.name[i], image: image, description: dummyDescription)
            self.promotions.append(promotion)
        }
    }
    
    private func registerCollectionCell() {
        let imageCell = UINib(nibName: "PromotionCollectionViewCell", bundle: nil)
        self.collectionView!.register(imageCell, forCellWithReuseIdentifier: "PromotionCollectionViewCell")
    }

    private func setupCollectionView() {
        let cellWidth : CGFloat = UIScreen.main.bounds.width / 2
        let cellHeight : CGFloat = UIScreen.main.bounds.height / 4
        let cellSize = CGSize(width: cellWidth , height:cellheight)

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PromotionsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return self.promotions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reusableCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromotionCollectionViewCell", for: indexPath)
        let cell: PromotionCollectionViewCell
        if let promotionCell = reusableCell as? PromotionCollectionViewCell {
            cell = promotionCell
        } else {
            cell = PromotionCollectionViewCell()
        }
        
        cell.setPromotionName(self.promotions[indexPath.row].name)
        cell.setImage(self.promotions[indexPath.row].image)

        return cell
    }

    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let promotionsDetailVC: PromotionDetailViewController = PromotionDetailViewController(nibName: "PromotionDetailViewController", bundle: nil)
        self.navigationController?.pushViewController(promotionsDetailVC, animated: true)
    }
}


public class PromotionModel {
    var name: String
    var image: UIImage?
    var description: String
    
    init(name: String, image: UIImage?, description: String) {
        self.name = name
        self.image = image
        self.description = description
    }
}
