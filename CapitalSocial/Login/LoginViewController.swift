//
//  LoginViewController.swift
//  CapitalSocial
//
//  Created by Alicia Castro on 3/22/20.
//  Copyright © 2020 Alicia Castro. All rights reserved.
//

import UIKit
import QRCodeReader

class LoginViewController: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    
    lazy var reader: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            let readerView = QRCodeReaderContainer(displayable: QRYourCustomView())
            $0.readerView = readerView
        }
      
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func scanQRCode(_ sender: Any) {
        self.navigationController?.pushViewController(reader, animated: true)
    }
    
    
    @IBAction func login(_ sender: Any) {
        guard let name = self.userNameTextField.text,
            let password = self.userPasswordTextField.text else {
            return
        }
        
//        Llamar al servicio de Auth
    }
    
}
