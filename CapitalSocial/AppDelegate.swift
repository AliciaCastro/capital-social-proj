//
//  AppDelegate.swift
//  CapitalSocial
//
//  Created by Alicia Castro on 3/22/20.
//  Copyright © 2020 Alicia Castro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let splashNavigationController: UINavigationController = self.getNavigationController()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.rootViewController = splashNavigationController
        self.window!.makeKeyAndVisible()
        return true
    }
}

extension AppDelegate {
    internal func getNavigationController() -> UINavigationController {
        let splashNavigationController: UINavigationController = UINavigationController()
        splashNavigationController.addChild(SplashViewController(nibName: "SplashViewController", bundle: nil))
        splashNavigationController.isNavigationBarHidden = true
        return splashNavigationController
    }
}

